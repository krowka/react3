import React, { createContext} from "react";


export const CharStats = createContext({
  name: "",
  str: 0,
  hp: 0,
  speed: 0,
});
export const UserStatsProvider = ({ children }) => {
  const name = localStorage.getItem('e');
  const str = 1;
  const hp = 1;
  const speed = 1;
  return (
    <CharStats.Provider
      value={{
        name,
        str,
        hp,
        speed,
      }}
    >
      {children}
    </CharStats.Provider>
  );
};
export const OponentStatsProvider = ({ children }) => {
  const name = "Demon";
  const str = 10;
  const hp = 12;

  return (
    <CharStats.Provider
      value={{
        name,
        str,
        hp,
      }}
    >
      {children}
    </CharStats.Provider>
  );
};
