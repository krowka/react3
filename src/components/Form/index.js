import React from "react";

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: "" };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    this.setState({ value: e.target.value });
  }
  // handleSubmit(e) {
  //   e.preventDefault();
  // }

  handleSubmit = (e) => {
    this.setItem(e);
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>Name: </label>
        <input
          name="name"
          type="text"
          value={this.state.value}
          onChange={(e) => this.handleChange(e)}
        />
        <br />
        <input name="send" type="submit" />
      </form>
    );
  }
}
export default Form;
