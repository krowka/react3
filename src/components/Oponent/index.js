import { useContext } from "react";
import { CharStats } from "../../context";

const Oponent = (props) => {
  const { name, str, hp, speed } = useContext(CharStats);
  return (
    <div className="col-6-1">
      <h1>{props.poziom}</h1>
      <p>Name: {name}</p>
      <p>Siła: {str}</p>
      <p>Hp: {hp}</p>
      <p>Prędkość: {speed}</p>
    </div>
  );
};
export default Oponent;
