import Form from "../Form";
import "./style.css";
import { useContext } from "react";
import { CharStats } from "../../context";

const User = (props) => {
  const showStats = true;
  const { name, str, hp, speed } = useContext(CharStats);
  return (
    <div className="col-6">
      {showStats ? (
        <div>
          <Form />
        </div>
      ) : (
        <div>
          <h1>{props.poziom}</h1>
          <p>Name: {name}</p>
          <p>Siła: {str}</p>
          <p>Hp: {hp}</p>
          <p>Prędkość: {speed}</p>
        </div>
      )}
    </div>
  );
};
export default User;
