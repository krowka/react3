import React from "react";
import User from "../../components/User";
import Oponent from "../../components/Oponent";

import { UserStatsProvider, OponentStatsProvider } from "../../context";
const Homepage = () => {
  return (
    <div>
      <UserStatsProvider>
        <User poziom="10" />
      </UserStatsProvider>

      <OponentStatsProvider>
        <Oponent poziom="10" />
      </OponentStatsProvider>
    </div>
  );
};

export default Homepage;